const port = 8080;

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  jfogs = require('gulp-jfogs'),
  sourcemaps = require('gulp-sourcemaps'),
  open = require('gulp-open'),
  connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    port: port,
    livereload: true,
    root: 'public'
  });
});

var src = [
  'public/js/boot/**/*.js',
  'public/js/utils/**/*.js',
  'public/js/achivments/**/*.js',
  'public/js/menu/**/*.js',
  'public/js/game/**/*.js',
  'public/js/app.js'
];

var productionSrc = [
  'public/**/*.html',
  'public/**/*.json',
  'public/**/*.png',
  'public/**/*.mp3',
  'public/**/*.wav',
  'public/**/*.ogg'
];

gulp.task('html', function() {
  gulp.src('public/**/*.html')
    .pipe(connect.reload());
});

gulp.task('js', function() {
  gulp.src(src)
    .pipe(sourcemaps.init())
    .on('error', function(err) {
      console.error('Error in compress task', err.toString());
    })
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write(/*'.', {
      // includeContent: false,
      // sourceRoot: '../js'
    }*/))
    .pipe(gulp.dest('public'))
    .pipe(connect.reload());
});

gulp.task('watch', function() {
  gulp.watch('public/js/**/*.js', ['js']);
  gulp.watch('public/**/*.html', ['html']);
});

gulp.task('production', function() {
  gulp.src(src)
    .on('error', function(err) {
      console.error('Error in compress task', err.toString());
    })
    .pipe(concat('app.js'))
    .pipe(jfogs())
    .pipe(uglify())
    .pipe(gulp.dest('production'));

  gulp.src(productionSrc)
    .pipe(gulp.dest('production'));
});

gulp.task('dev', function() {
  gulp.src(src)
    .on('error', function(err) {
      console.error('Error in compress task', err.toString());
    })
    .pipe(concat('app.js'))
    .pipe(gulp.dest('production'));

  gulp.src(productionSrc)
    .pipe(gulp.dest('production'));
});

gulp.task('open', function() {
  gulp.src(__filename)
  .pipe(open({uri: 'http://localhost:' + port, app: 'chrome'}));
})

gulp.task('default', ['js', 'connect', 'watch']);