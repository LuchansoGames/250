var FullScreen = function(game) {
  this.game = game;
}

FullScreen.isFullScreen = false;

FullScreen.prototype = {
  preload: function() {
    this.game.load.spritesheet('fullscreen', 'img/ui/ic-fullscreen.png', 192, 192);
  },

  create: function() {
    this.btn = this.game.add.button(this.game.world.width, this.game.world.height, 'fullscreen', this.fullscreen_click, this, 0, 0, 0);
    this.btn.width = 50;
    this.btn.height = 50;
    this.btn.anchor.setTo(1);

    if (FullScreen.isFullScreen) {
      this.btn.setFrames(1, 1, 1);
    }
  },

  fullscreen_click: function() {
    if (this.btn.frame === 0) {
      this.btn.setFrames(1, 1, 1);
      game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
      game.scale.startFullScreen(true);
      FullScreen.isFullScreen = true;
    } else {
      this.btn.setFrames(0, 0, 0);
      game.scale.stopFullScreen();
      FullScreen.isFullScreen = false;
    }
  }
}