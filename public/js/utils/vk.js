VK.wall = {
  postAchivment: function(photo) {
    VK.api("wall.post", {"message": "Открыто новое достижение в игре \"Хватай и Беги!\"\nhttps://vk.com/app5448474", "attachments": photo}, function (data) {
  });
  }
}

var vkPreroll = {
  run: function() { }
};

function isVkEnv() {
  return (location.ancestorOrigins.length !== 0 && location.ancestorOrigins[0].indexOf('vk') !== -1);
}

function ADSOnLoad(callback) {
  var adsTimer = setInterval(function() {
    var isLoaded = document.getElementById('vk_ads_75686').style.background === 'none';

    if (isLoaded) {
      clearInterval(adsTimer);
      callback();
    }
  }, 3000);
}

function onLoad() {
  document.getElementById('vk_ads_75686').style['max-height'] = '';
}

setTimeout(function() {
  if (!isVkEnv()) {
    return;
  }

  var adsParams = {
    "ad_unit_id": 75686,
    "ad_unit_hash": "232dff1590ac9d07125fe39844d8d38a"
  };

  function vkAdsInit() {
    ADSOnLoad(onLoad);
    VK.Widgets.Ads('vk_ads_75686', {}, adsParams);

    VK.init({
      apiId: 5514423
    });

    var user_id = null;
    var app_id = 5448474;
    vkPreroll = new VKAdman();
    vkPreroll.run = function() {
      vkPreroll.setupPreroll(app_id, {preview: 8});
    }
    admanStat(app_id, user_id);    
  }

  if (window.VK && VK.Widgets) {
    vkAdsInit();
  } else {
    if (!window.vkAsyncInitCallbacks) window.vkAsyncInitCallbacks = [];
    vkAsyncInitCallbacks.push(vkAdsInit);
    var protocol = ((location.protocol === 'https:') ? 'https:' : 'http:');
    var adsElem = document.getElementById('vk_ads_75686');
    var scriptElem = document.createElement('script');
    scriptElem.type = 'text/javascript';
    scriptElem.async = true;
    scriptElem.src = protocol + '//vk.com/js/api/openapi.js?121';
    adsElem.parentNode.insertBefore(scriptElem, adsElem.nextSibling);
  }

}, 0);