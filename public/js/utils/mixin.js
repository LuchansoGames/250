function mixIt(baseClass, mixin) {
  for (var key in mixin) baseClass.prototype[key] = mixin[key];
}