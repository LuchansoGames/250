var GameStateNew = {
  init: function(lvl) {
    this.game.stage.backgroundColor = Store.backgroundColor;

    Cache.lvl = this.lvl = lvl;
    this.isPause = false;

    this.soundManager = new SoundManager.getInstance(this.game);
    score = this.scoreManager = new ScoreManager.getInstance(this.game);
    this.screenshoot = new Screenshoot(this.game);
    this.enemyBulder = new EnemyBulder(this.game);
    this.pauseMenu = new PauseMenu(this.game);
    this.achivment = new Achivment(this.game);
    this.controll = new Controll(this.game);
    this.border = new Border(this.game, this.lvl);
    this.square = new Square(this.game, this.border, this.soundManager);
    this.coin = new Coin(this.game, this.border, this.soundManager);
    this.ui = new UI(this.game, this.soundManager);
    this.enemySpawnBulder = new EnemySpawnBulder(this.game, this.lvl, this.border, this.enemyBulder);

    this.pauseMenu.onShowMainMenu.add(this.onShowMainMenu, this);
    this.pauseMenu.onRestart.add(this.restartGame, this);
    this.pauseMenu.onHide.add(this.resume, this);

    this.game.onBlur.add(this.focusLost, this);
  },

  preload: function() {
    this.enemySpawnBulder.preload();
    this.soundManager.preload();
    this.enemyBulder.preload();
    this.pauseMenu.preload();
    this.achivment.preload();
    this.border.preload();
    this.square.preload();
    this.coin.preload();
    this.ui.preload();
  },

  create: function() {
    this.soundManager.create();
    this.enemyBulder.create();
    this.pauseMenu.create();
    this.border.create();
    this.square.create();
    this.coin.create();
    this.ui.create();
    this.enemySpawnBulder.create();

    this.refreshUi();

    this.controll.create({
      up: function() { this.square.move(Square.directionType.UP) },
      down: function() { this.square.move(Square.directionType.DOWN) },
      left: function() { this.square.move(Square.directionType.LEFT) },
      right: function() { this.square.move(Square.directionType.RIGHT) }
    }, this);

    this.addEventsListener();

    if (!Settings.isMuted && !this.soundManager.music.firstPlay) {
      this.soundManager.music.play();
      this.soundManager.music.firstPlay = false;
    }
  },

  refreshUi: function() {
    this.ui.setScore(this.scoreManager.score);
    this.ui.updateRatio(this.scoreManager.ratio);
    this.updateProgressBar();
  },

  focusLost: function() {
    this.pause(true);
  },

  update: function() {
    if (this.isPause) {
      return;
    }

    if (this.overlap(this.square.sprite, this.coin.sprite)) {
      this.coin.take();

      this.ui.setScore(this.scoreManager.takeCoin());
      this.ui.updateRatio(this.scoreManager.ratio);
      this.sheckNextLvl();
      this.updateProgressBar();
    }

    for (var i = 0; i < this.enemyBulder.enemys.length; i++) {
      enemy = this.enemyBulder.enemys[i];
      if (this.overlap(enemy.sprite, this.square.sprite)) {
        this.soundManager.dieSoundPlay();
        enemy.die();

        this.ui.setScore(this.scoreManager.loseScore());
        this.ui.updateRatio(this.scoreManager.ratio);
        this.sheckNextLvl();
        this.updateProgressBar();
      }
    }
  },

  updateProgressBar: function() {
    var needScore = this.scoreManager.calcScoreByLvl(this.lvl);
    var progress = this.scoreManager.score / needScore;
    this.ui.progressBar.setValue(progress);
  },

  sheckNextLvl: function() {
    if (this.lvlIsWin() && Store.maxLvl > this.lvl) {
      this.nextLvl();
    }
  },

  lvlIsWin: function() {
    return this.scoreManager.score > this.scoreManager.calcScoreByLvl(this.lvl);
  },

  addEventsListener: function() {
    this.ui.onPauseButtonClick.add(function() {
      this.pause(true);
    }, this);
  },

  nextLvl: function() {
    this.pause();

    this.screenFadeInWhite()
    .then((function() {
      this.game.state.start('NewLvlScreen', true, false, this.lvl + 1);
    }).bind(this));
  },

  overlap: function(obj1, obj2) {
    return Phaser.Rectangle.intersects(obj1.getBounds(), obj2.getBounds());
  },

  render: function() {
    this.screenshoot.render();
  },

  pause: function(isShowPauseMenu) {
    this.game.onBlur.remove(this.focusLost, this);
    this.game.physics.arcade.isPaused = this.isPause = true;

    this.enemySpawnBulder.pause();
    this.ui.pause();

    this.square.pause();
    this.coin.pause();

    if (isShowPauseMenu)
      this.pauseMenu.show(this.scoreManager.score);
  },

  resume: function() {
    this.game.onBlur.add(this.focusLost, this);
    this.game.physics.arcade.isPaused = this.isPause = false;

    this.enemySpawnBulder.resume();
    this.ui.resume();

    this.square.resume();
    this.coin.resume();

    this.pauseMenu.hide();
  },

  onShowMainMenu: function() {
    this.resume();
    this.soundManager.music.pause();
    this.game.state.start('Menu', true, false, this.lvl);
  },

  restartGame: function() {
    this.resume();
    this.scoreManager.reset();
    this.game.state.restart(true, false, 1);
  },

  screenFadeInWhite: function() {
    return new Promise((function(res, rej) {
      this.pause();

      var graphics = this.game.add.graphics(0, 0);
      graphics.beginFill(0xFFFFFF);
      graphics.drawRect(0, 0, this.game.width, this.game.height);
      graphics.alpha = 0;

      this.game.add.tween(graphics).to({alpha: 1}, 750).start().onComplete.add(function() {
        res();
      }, this);
    }).bind(this));
  },

  shutdown: function() {
    this.game.onBlur.remove(this.focusLost, this);
    this.resume();
  }
}