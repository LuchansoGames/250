EnemyBulder = function(game) {
  this.game = game;
  this.enemys = [];
}

EnemyBulder.prototype = {
  preload: function() {
    this.game.load.image('enemy', 'img/game/enemy.png');
    this.game.load.image('enemy-particle', 'img/game/enemy-particle.png');
  },

  create: function() {
    this.emitter = this.game.add.emitter(0, 0);

    this.emitter.makeParticles('enemy-particle');
    this.emitter.setXSpeed(-150, 150);
    this.emitter.setYSpeed(-150, 150);
    this.emitter.setScale(2, 0, 2, 0, 800);
  },

  add: function(x, y, velocity) {
    var enemy = new Enemy(this.game, x, y, velocity, this);
    this.enemys.push(enemy);
    return enemy;
  }
}