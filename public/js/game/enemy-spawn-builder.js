EnemySpawnBulder = function(game, lvl, border, enemyBulder) {
  this.game = game;
  this.lvl = lvl;
  this.border = border;
  this.enemyBulder = enemyBulder;
  this.spawns = [];
}

EnemySpawnBulder.prototype = {
  preload: function() {
    this.game.load.image('enemySpawn', 'img/game/enemy-spawn.png');
    this.game.load.json('lvl-' + this.lvl, 'maps/lvl-' + this.lvl + '.json');
  },

  create: function() {
    var rowSpawns = this.game.cache.getJSON('lvl-' + this.lvl).enemySpawns;    

    this.spawns = rowSpawns.map(function(spawn) {
      return new EnemySpawn(
        this.game,
        spawn,
        this.border,
        this.enemyBulder
      );
    }, this);
  },

  pause: function() {
    this.spawns.forEach(function(spawn) {
      spawn.pause();
    });
  },

  resume: function() {
    this.spawns.forEach(function(spawn) {
      spawn.resume();
    });
  }
}