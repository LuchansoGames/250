var Enemy = function(game, x, y, velocity, enemyBulder) {
  this.game = game;
  this.enemyBulder = enemyBulder;

  this.sprite = this.game.add.sprite(x, y, 'enemy');
  this.game.physics.arcade.enable([this.sprite]);
  this.sprite.body.velocity = velocity;
  this.sprite.outOfBoundsKill = true;
  this.sprite.checkWorldBounds = true;

  this.sprite.anchor.setTo(0.5, 0.5);

  this.sprite.events.onOutOfBounds.add(this.destroy, this);
}

Enemy.prototype = {
  destroy: function() {
    var position = this.enemyBulder.enemys.indexOf(this);
    this.enemyBulder.enemys.splice(position, 1);
  },

  die: function() {
    this.sprite.destroy();
    this.destroy();

    this.enemyBulder.emitter.x = this.sprite.x;
    this.enemyBulder.emitter.y = this.sprite.y;
    this.enemyBulder.emitter.start(true, 800, null, 20);
  }
};