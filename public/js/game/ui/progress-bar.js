var ProgressBar = function(game, color) {
  this.game = game;
  this.value = 0;
  this.color = color || Store.progressBarColor;
}

ProgressBar.prototype = {
  preload: function() {
    this.game.load.image('progress-bar', 'img/ui/progress-bar.png');
    this.game.load.image('progress-bar-head', 'img/ui/progress-bar-head.png');
  },

  create: function() {
    this.body = this.game.add.sprite(0, 0, 'progress-bar');
    this.head = this.game.add.sprite(this.body.width, 0, 'progress-bar-head');

    this.body.tint = this.color;
    this.head.tint = this.color;

    this.minWidth = this.body.width;
    this.maxWidth = this.game.width - this.body.width - this.minWidth - 50;

    this.setValue(0);
  },

  setValue: function(value) {
    this.value = value;

    var bodyWidth = this.minWidth + this.value * this.maxWidth;

    this.game.add.tween(this.body)
    .to({width: bodyWidth}, 1000, Phaser.Easing.Exponential.Out)
    .start();

    this.game.add.tween(this.head)
    .to({x: bodyWidth}, 1000, Phaser.Easing.Exponential.Out)
    .start();
  }
}