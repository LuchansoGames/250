var ScoreManagerClass = function(game) {
  this.game = game;
  this.score = 0;
  this.ratio = 1.0;

  this.ratioSaveTime = 1.0;

  this.coinScoreAdd = 15;
  this.coinRatioAdd = 0.075;
  this.ratioSaveTimeAdd = 0.05;
  this.ratioSaveTimeSub = 0.01;

  this.coinsTaked = 0;
}

ScoreManagerClass.prototype = {
  takeCoin: function() {
    this.score += this.coinScoreAdd * this.ratio;
    this.ratio += this.coinRatioAdd;

    this.coinsTaked++;

    return this.score;
  },

  addRatioSaveTime: function() {
    this.ratioSaveTime += this.ratioSaveTimeAdd;
  },

  subRatioSaveTime: function() {
    this.ratioSaveTime -= this.ratioSaveTimeSub;
  },

  loseScore: function() {
    this.score /= 2;
    this.ratio = 1 + (this.ratio - 1) / 2;
    
    return this.score;
  },

  loseRateByTime: function() {
    this.ratio = 1;

    return this.ratio;
  },

  doubleRatio: function() {
    this.ratio *= 2;

    return this.ratio;
  },

  reset: function() {
    this.ratio = 1;
    this.score = 0;
    this.coinsTaked = 0;
  },

  calcScoreByLvl: function(lvl) {
    var result = 1000 + 500 * ((2 + lvl) * (lvl - 1)) / 2;
    return result;
  },
}

var ScoreManager = (function() {
  var instance;

  function createInstance(game) {
    var scoreManager = new ScoreManagerClass(game);
    return scoreManager;
  }

  return {
    getInstance: function(game) {
      if (!instance) {
        instance = createInstance(game);
      }
      return instance;
    }
  };

})();