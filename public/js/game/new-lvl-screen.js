NewLvlScreen = {
  init: function(nextLvl) {
    Cache.lvl = this.nextLvl = nextLvl;
    this.onHide = new Phaser.Signal();
    this.scoreManager = ScoreManager.getInstance();    
  },

  preload: function() {
    this.game.load.image('play', 'img/ui/ic-play.png');
  },

  create: function() {
    this.game.stage.backgroundColor = 0xFFFFFF;

    this.runPreloading(this.nextLvl);

    this.addLableLvl();
    this.addScoreLable();
    this.addLoadLable();
    this.addProgressBarLable();

    vkPreroll.run();
  },

  addLableLvl: function() {
    var style = {
      font: "45px Jura",
      fill: "#333"
    }

    var lvlLable = this.game.add.text(this.game.world.centerX, 15, 'Уровень ' + this.nextLvl, style);
    lvlLable.anchor.set(0.5, 0);
  },

  addScoreLable: function() {
    var style = {
      font: "26px Jura",
      fill: "#333"
    }

    var scoreLable = this.game.add.text(this.game.world.centerX, 60, 'Очки ' + Math.round(this.scoreManager.score), style);
    scoreLable.anchor.set(0.5, 0);
  },

  addLoadLable: function() {
    var style = {
      font: "45px Jura",
      fill: "#333"
    }

    this.loadLable = this.game.add.text(this.game.world.centerX, this.game.world.centerY, 'Загрузка уровня...', style);
    this.loadLable.anchor.set(0.5);
  },

  addProgressBarLable: function() {
    var style = {
      font: "40px Jura",
      fill: "#333"
    }

    this.progressBar = this.game.add.text(this.game.world.centerX, this.game.world.centerY + 50, '0%', style);
    this.progressBar.anchor.set(0.5);
    this.progressBar.progress = 0;
    this.progressBar.lastValue = 0;

    this.nextTick();
  },

  addButton: function() {
    this.btnPlay = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'play', this.runNextLvl, this);
    this.btnPlay.anchor.set(0.5);
    this.btnPlay.alpha = 0;
    this.btnPlay.tint = Store.coinColor;

    var style = {
      font: "25px Jura",
      fill: "#" + Store.coinColor.toString(16)
    }

    var btnText = this.game.add.text(this.game.world.centerX, this.game.world.centerY + this.btnPlay.height / 2, 'Продолжить', style);
    btnText.anchor.setTo(0.5);
    btnText.alpha = 0;

    this.btnPlay.events.onInputOver.add(function() {
      this.game.add.tween(btnText).to({alpha: 1}, 100).start();
    }, this)

    this.btnPlay.events.onInputOut.add(function() {
      this.game.add.tween(btnText).to({alpha: 0}, 100).start();
    }, this)

    this.game.add.tween(this.btnPlay).to({alpha: 1}, 100).delay(150).start();
  },

  nextTick: function() {
    this.progressBar.progress += this.game.rnd.frac() * 0.06;

    this.updateProgressBar();

    if (this.progressBar.progress < 1) {
      setTimeout(
        this.nextTick.bind(this),
        this.game.rnd.between(10, 100)
      );
    } else {
      this.done();
    }
  },

  updateProgressBar: function() {
    this.game.add.tween(this.progressBar).to({lastValue: this.progressBar.progress}, 100).onUpdateCallback(function() {
      this.progressBar.text = this.filterData(this.progressBar.lastValue) + "%";
    }, this).start();
  },

  filterData: function(number) {
    return Math.min(Math.round(number * 1000) / 10, 100);
  },

  show: function() {
    var graphics = this.game.add.graphics(0, 0);
    graphics.beginFill(0xFFFFFF);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.alpha = 0;

    this.game.add.tween(graphics).to({alpha: 1}, 750).start();
  },

  hide: function() {
    this.onHide.dispatch();
    this.game.add.tween(graphics).to({alpha: 0}, 750).start();
  },

  done: function() {
    this.game.add.tween(this.loadLable).to({alpha: 0}, 100).start();
    this.game.add.tween(this.progressBar).to({alpha: 0}, 100).start();

    this.addButton();
  },

  runPreloading: function(lvl) {
    var loader = new Phaser.Loader(this.game);

    loader.json('lvl-' + lvl, 'maps/lvl-' + lvl + '.json');
    loader.start();
  },

  runNextLvl: function() {
    this.game.state.start('Game.v2', true, false, this.nextLvl);
  }
}