var Store = { };

Store.maxLvl = 2;

Store.width = 1000;
Store.height = 900;

Store.squareMargin = 20;
Store.playerColor = 0xEEFF41;
Store.squareMoveTime = 75;
Store.squareSize = 50;
Store.moveDistance = Store.squareSize + Store.squareMargin;

Store.borderColor = 0x1A237E;
Store.borderLineWidth = 5;

Store.coinColor = 0x2196F3;
Store.coinInterval = 500;
Store.coinSize = 24;
Store.coinInterval = 500;

Store.backgroundColor = 0x283593;

Store.terrainSize = (Store.moveDistance + Store.squareMargin);

Store.pauseMenuBtnsSize = 150;

Store.progressBarColor = 0xFF9800;