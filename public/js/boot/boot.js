var Boot = {
  create: function() {
    if (!isVkEnv()) {
      this.game.scale.pageAlignHorizontally = true;
      this.game.scale.pageAlignVertically = true;
    }

    Settings.load();

    RunGame();
  }
}