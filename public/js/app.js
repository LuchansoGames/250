var game = new Phaser.Game(Store.width, Store.height, Phaser.AUTO, 'game');

game.state.add('Boot', Boot);
game.state.add('Game.v2', GameStateNew);
game.state.add('Menu', Menu);
game.state.add('NewLvlScreen', NewLvlScreen);
game.state.add('Achivments', AchivemtnsPage);

function RunGame() {
  game.state.start('Achivments');
  // game.state.start('Game.v2', true, false, 1);
  // game.state.start('Menu', true, false, 1);
  // game.state.start('NewLvlScreen', true, false, 1);
}

game.state.start('Boot');