var ScoreRow = function(game, x, y, userimage, username, url, score, position) {
  this.game = game;
  this.url = url;
  this.score = score;
  this.username = username;
  this.userimage = userimage;
  this.x = x;
  this.y = y;
  this.position = position.toString();

  this.addBorder();
  this.addLabels();
}

ScoreRow.prototype = {
  click: function() {
    window.open(this.url, '_blank');
  },

  addBorder: function() {
    var borders = this.game.make.sprite(0, 0, 'score-row');
    var filter = this.game.make.sprite(0, 0, 'score-filter');
    this.avatar = this.game.make.sprite(0, 0, this.userimage);

    var bitmap = new Phaser.BitmapData(this.game, null, borders.width, borders.height);

    bitmap.draw(this.avatar);
    bitmap.draw(filter);
    bitmap.draw(borders);

    this.sprite = this.game.make.image(this.x, this.y, bitmap);
    this.sprite.inputEnabled = true;
    this.sprite.input.useHandCursor = true;
    this.sprite.events.onInputUp.add(this.click, this);
    this.sprite.events.onInputOver.add(this.over, this);
    this.sprite.events.onInputOut.add(this.out, this);

    this.hoverEffeect = this.game.make.image(this.sprite.width - 15, 0, 'score-hover-effect');
    this.hoverEffeect2 = this.game.make.image(35, 0, 'score-hover-effect-2');

    this.hoverEffeect.alpha = 0;
    this.hoverEffeect2.alpha = 0;

    this.sprite.addChild(this.hoverEffeect);
    this.sprite.addChild(this.hoverEffeect2);
  },

  over: function() {    
    if (this.hoverEffeectFadeOut)
      this.hoverEffeectFadeOut.stop();

    if (this.hoverEffeect2FadeOut)
      this.hoverEffeect2FadeOut.stop();

    if (this.hoverTween2)
      this.hoverTween2.stop();
    
    this.hoverEffeect.alpha = 1;
    this.hoverEffeect2.alpha = 1;
    this.hoverEffeect2.x = 35;

    this.hoverTween2 = this.game.add.tween(this.hoverEffeect2).to({x: this.sprite.width - 15}, 5000).start();
    this.hoverTween2.repeat(-1, 0);
  },

  out: function() {
    this.hoverEffeectFadeOut = this.game.add.tween(this.hoverEffeect).to({alpha: 0}, 500).start();    
    
    this.hoverEffeect2FadeOut = this.game.add.tween(this.hoverEffeect2).to({alpha: 0}, 500).start();
  },

  addLabels: function() {
    var usernameStyle = {
      font: "17px Jura",
      fill: "#fff",
      fontWeight: "bold",
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }

    var scoreStyle = {
      font: "17px Jura",
      fill: "#FFEB3B",
      fontWeight: "bold",
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }

    var positionStyle = {
      font: "30px Jura",
      fill: "#2196F3",
      fontWeight: "bold",
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }

    this.lableUsername = this.game.make.text(this.avatar.width + 5, 3, this.username, usernameStyle);

    this.lableScore = this.game.make.text(this.avatar.width + 5, 0, this.score, scoreStyle);
    this.lableScore.y = this.lableUsername.y + this.lableUsername.height;
    
    this.lablePosition = this.game.make.text(this.sprite.width - 15, 10, this.position, positionStyle)
    this.lablePosition.anchor.setTo(1, 0);

    this.sprite.addChild(this.lableUsername);
    this.sprite.addChild(this.lableScore);
    this.sprite.addChild(this.lablePosition);
  },

  hide: function() {
    this.game.tween(this).to({alpha: 0}, 100).start();
  },

  show: function() {
    this.game.tween(this).to({alpha: 1}, 100).start();
  }
}