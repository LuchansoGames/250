var ScoreBuilder = function(game, x, y) {
  this.game = game;
  this.rows = [];
  /**
   * Скорость скролинга, 50 пикселей в 2 секунды (25px/sec)
   * @type {[type]}
   */
  this.scrollSpeed = 50 / 2000;
  this.x = x;
  this.y = y;
  this.fadeTween = null;
}

ScoreBuilder.basePositionY = 125;
ScoreBuilder.startScrollingDelay = 1000;

ScoreBuilder.prototype = {
  preload: function() {
    this.game.load.image('score-row', 'img/menu/score-row.png');
    this.game.load.image('score-filter', 'img/menu/score-filter.png');
    this.game.load.image('score-hover-effect', 'img/menu/score-hover-effect.png');
    this.game.load.image('score-hover-effect-2', 'img/menu/score-hover-effect-2.png');
  },

  create: function() {
    this.backgroundScoreRow = this.game.make.image(0, 0, 'score-row');
    var rowBitmap = new Phaser.BitmapData(this.game, null, this.backgroundScoreRow.width, 1);
    this.tableRatingSprite = this.game.add.sprite(this.x - this.backgroundScoreRow.width / 2, this.y, rowBitmap);    

    this.addLableTop100();
  },

  add: function(userimage, username, url, score, position) {
    var y = 0;

    if (this.rows.length === 0) {
      y = ScoreBuilder.basePositionY;
    } else {
      y = this.rows[this.rows.length - 1].sprite.y + this.backgroundScoreRow.height;
    }

    var scoreRow = new ScoreRow(this.game, 0, y, userimage, username, url, score, position);

    this.rows.push(scoreRow);
    this.tableRatingSprite.addChild(scoreRow.sprite);

    return scoreRow;
  },

  addPlayerScore: function(userimage, username, url, score, position) {
    var scoreRow = new ScoreRow(this.game, 0, 0, userimage, username, url, score, position);

    this.tableRatingSprite.addChild(scoreRow.sprite);
    this.add(userimage, username, url, score, position);

    return scoreRow;
  },

  runScrolling: function() {    
    this.rows.forEach(function(scoreRow) {
      this.addScrolling(scoreRow.sprite, ScoreBuilder.startScrollingDelay);
    }, this);
  },

  addScrolling: function(sprite, delay) {
    delay = delay || 0;

    this.addSpeed(sprite, delay);
    this.addFadeOutTrigger(sprite, delay);
  },

  addSpeed: function(sprite, delay) {
    var timeScrolling = (sprite.y / this.scrollSpeed);

    this.game.add.tween(sprite).to({
      y: 0
    }, timeScrolling).delay(delay).start();
  },

  addFadeOutTrigger: function(sprite, delay) {
    var timeTrigger = ((sprite.y - ScoreBuilder.basePositionY) / this.scrollSpeed);

    this.game.add.tween(sprite).to({
      alpha: 0
    }, 1000).delay(timeTrigger + delay).start().onComplete.add(function() {
      sprite.inputEnabled = false;
    }, this);
  },

  addLableTop100: function() {
    var style = {
      font: '21px Jura',
      fontWeight: 'Bold',
      fill: '#fff'
    }
    
    var lable = this.game.make.text(this.tableRatingSprite.width / 2, ScoreBuilder.basePositionY / 2, 'Рейтинг - TOP 100', style);
    lable.anchor.setTo(0.5, 0);
    
    this.tableRatingSprite.addChild(lable);
  }
}