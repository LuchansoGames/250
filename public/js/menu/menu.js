var Menu = {
  init: function(lvl) {
    this.score = new ScoreBuilder(this.game, this.game.world.centerX, game.world.centerY - ScoreBuilder.basePositionY);
    this.fullScreen = new FullScreen(this.game);
    this.soundManager = SoundManager.getInstance(this.game);
    Cache.lvl = this.lvl = lvl;
  },

  preload: function() {    
    this.game.load.crossOrigin = true;
    this.game.load.image('logo', 'img/ui/logo.png');
    this.game.load.image('play', 'img/ui/ic-play.png');
    this.game.load.image('help', 'img/ui/ic-help.png');
    this.game.load.image('list', 'img/ui/ic-list.png');
    this.game.load.image('unknown-user', 'img/ui/unknown-user.png');
    this.game.load.spritesheet('volume', 'img/ui/volume-spritesheet.png', 96, 96);
    this.game.load.json('rating-data', 'api/hydra/rating');

    this.soundManager.preload();

    this.score.preload();
    this.fullScreen.preload();
  },

  create: function() {
    this.game.stage.backgroundColor = Store.backgroundColor;

    var rating = this.game.cache.getJSON('rating-data').users;

    this.score.create();

    this.addVolumeButton();
    this.addLogotype();
    this.addControls();
    this.addScoreTable(rating);
    this.fullScreen.create();

    this.soundManager.create();
  },

  update: function() {
  },

  volumeButton_click: function() {
    if (this.isPause)
      return

    if (this.soundButton.frame === 0) {
      Settings.isMuted = false;
      this.soundButton.setFrames(1, 1, 1);
    } else if (this.soundButton.frame === 1) {
      Settings.isMuted = true;
      this.soundButton.setFrames(0, 0, 0);
    }
  },

  addVolumeButton: function() {
    var frames = 1;

    if (Settings.isMuted) {
      frames = 0;
    }

    this.soundButton = this.game.add.button(5, 5, 'volume', this.volumeButton_click, this, frames, frames, frames);
    this.soundButton.width = 48;
    this.soundButton.height = 48;
  },

  addLogotype: function() {
    var logoX = game.world.centerX;
    var logoY = 75;

    this.logotype = game.add.sprite(logoX, logoY, 'logo');
    this.logotype.scale.setTo(0.5);
    this.logotype.anchor.setTo(0.5);
    this.logotype.alpha = 0;

    this.game.add.tween(this.logotype).to({
      alpha: 1
    }, 250).start();
  },

  addControls: function() {
    this.buttonDistance = 250;
    this.buttonSize = 125;
    this.buttonLableStyle = {
      font: "24px Jura",
      fill: "#fff",
      boundsAlignH: "center",
      boundsAlignV: "middle"
    };
    this.btnLablepadding = 85;

    this.buttonsGroup = this.game.add.group();

    this.buttonsGroup.add(this.createNewBtn(this.game.world.centerX - this.buttonDistance, 0, 'play', 'Играть', this.btnPlay_click));
    this.buttonsGroup.add(this.createNewBtn(this.game.world.centerX, 0, 'help', 'Как играть?', this.btnHelp_click));
    this.buttonsGroup.add(this.createNewBtn(this.game.world.centerX + this.buttonDistance, 0, 'list', 'Достижения', this.btnAchivments_click));
    this.buttonsGroup.y = this.logotype.y + 125;

    this.buttonsGroup.alpha = 0;
    this.game.add.tween(this.buttonsGroup).to({
      alpha: 1
    }, 250).delay(150).start();
  },

  createNewBtn: function(x, y, name, text, callback) {
    var btnBox = this.game.add.group();

    var btnX = x;
    var btnY = y;

    var btn = this.game.add.button(btnX, btnY, name, callback, this);
    btn.anchor.set(0.5, 0.5);
    btn.width = this.buttonSize;
    btn.height = this.buttonSize;

    var btnLable = this.game.add.text(btnX, btnY + this.btnLablepadding, text, this.buttonLableStyle);
    btnLable.anchor.set(0.5, 0.5);

    btn.onInputOver.add(this.btnOver, btnLable);
    btn.onInputOut.add(this.btnOut, btnLable);

    btnLable.alpha = 0;

    btnBox.addMultiple([btn, btnLable]);

    return btnBox;
  },

  btnPlay_click: function() {
    this.game.state.start('Game.v2', true, false, this.lvl);
  },

  btnHelp_click: function() {

  },

  btnAchivments_click: function() {
    Cache.lvl = this.lvl;

    this.game.state.start('Achivments');
  },

  btnOver: function() {
    game.add.tween(this).to({
      alpha: 1
    }, 100).start();
  },

  btnOut: function() {
    game.add.tween(this).to({
      alpha: 0
    }, 100).start();
  },

  addScoreTable: function(data) {
    var loader = new Phaser.Loader(this.game);
    loader.crossOrigin = true;

    data.forEach(function(row) {
      var userimage = 'img-id' + row.id;
      loader.image(userimage, row.avatar);

      var loaded = function(userimage) {
        if (row.isPlayer) {
          this.score.addPlayerScore(userimage, row.username, row.url, row.score, row.position);
        } else {
          this.score.add(userimage, row.username, row.url, row.score, row.position);
        }
      }

      loader.onLoadComplete.add(function() {
        loaded.bind(this)(userimage);
      }, this);

      loader.onFileError.add(function() {
        loaded.bind(this)('unknow-user');
      }, this);
    }, this);

    loader.start();
    loader.onLoadComplete.add(function() {
        this.score.runScrolling();
    }, this);
  }
}