var AchivemtnsPage = {
  preload: function() {
    this.game.load.image('achivments-row', 'img/achivments/achivments-row.png');
    this.game.load.image('achivments-row-filter', 'img/achivments/achivments-filter.png');
    this.game.load.image('achivments-unlocked', 'img/achivments/achivments-unlocked.png');
    this.game.load.image('ach+1', 'img/achivments/ach+1.png');
    this.game.load.image('btnShareVk', 'img/ui/button-share-vk.png');
    this.game.load.image('menu', 'img/ui/ic-reply.png');
    this.game.load.image('ic-up', 'img/ui/ic-up.png');
    this.game.load.image('ic-down', 'img/ui/ic-down.png');
  },

  create: function() {
    this.achivmentsList = [];
    this.game.stage.backgroundColor = Store.backgroundColor;

    this.topInterval = 110;
    this.marginTop = 210;

    this.addBtnMenu();

    this.addAchivments(true, 'photo-123966610_419178836', 'ach+1', 'Страница с ачивками', 'Необходимо сделать страницу, на которой будут отображаться будующие ачивки');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Поделиться', 'Добавить возможность поделиться результатом на стену');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Достижения', 'Придумать достижения и задания');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Иконки', 'Нарисовать иконки для ачивок');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Скролл', 'Добавить скролл к ачивкам');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Скролл', 'Добавить скролл к ачивкам');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Скролл', 'Добавить скролл к ачивкам');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Скролл', 'Добавить скролл к ачивкам');
    this.addAchivments(false, 'photo-123966610_419178836', 'ach+1', 'Социальщина', 'Добавить социальные механики');

    this.addScrollBtns();
  },

  addBtnMenu: function() {
    this.btnMenu = this.game.add.button(this.game.world.centerX, 15, 'menu', this.goToMenu, this);
    this.btnMenu.width = 125;
    this.btnMenu.height = 125;
    this.btnMenu.anchor.setTo(0.5, 0);

    var style = {
      font: "21px Jura",
      fill: "#fff"
    }

    var text = this.game.add.text(this.game.world.centerX, this.btnMenu.y + this.btnMenu.height, 'В меню', style);
    text.anchor.set(0.5, 0);
  },

  addScrollBtns: function() {
    this.btnUp = this.game.add.button(this.game.width - 25, this.game.height - 50 - 25, 'ic-up', this.btnUp_click, this);
    this.btnDown = this.game.add.button(this.game.width - 25, this.game.height - 25, 'ic-down', this.btnDown_click, this);

    this.btnUp.anchor.setTo(1);
    this.btnUp.scale.setTo(0.6);
    
    this.btnDown.anchor.setTo(1);
    this.btnDown.scale.setTo(0.6);
  },

  btnDown_click: function() { 
    this.scroll(-250);
  },

  btnUp_click: function() {
    this.scroll(250);
  },

  goToMenu: function() {
    this.game.state.start('Menu', true, false, Cache.lvl);
  },

  render: function() {
    this.game.debug.spriteBounds(this.btnDown);
  },

  scroll: function(delta) {
    var first = this.achivmentsList[0];
    var last = this.achivmentsList[this.achivmentsList.length - 1];

    if (first.y + delta >= this.marginTop && delta > 0) {
      delta = this.marginTop - first.y;
    }
    if ((last.y + last.height)  + delta <= this.game.height && delta < 0) {
      delta = -((last.y + last.height) - this.game.height);
    }

    this.achivmentsList.forEach(function(ach) {
      this.game.add.tween(ach).to({y: ach.y + delta}, 125).start();
      if (ach.y + delta < this.marginTop - 50) {
        this.game.add.tween(ach).to({alpha: 0}, 125).start();
      } else if (ach.alpha === 0) {
        this.game.add.tween(ach).to({alpha: 1}, 125).start();
      }
    }, this);
  },

  addAchivments: function(unclocked, photo, name, title, description) {
    this.topInterval = 110;
    this.marginTop = 210;

    var achivment = new AchivmentsRow(this.game, 0, 0, name, unclocked, title, description, photo);
    achivment.x = this.game.world.centerX - achivment.width / 2;
    achivment.y = this.marginTop + this.topInterval * this.achivmentsList.length;

    this.achivmentsList.push(achivment);
  }
}