var AchivmentsRow = function(game, x, y, name, unlocked, title, description, photo) {
  Phaser.Sprite.apply(this, [game, x, y, 'achivments-row']);

  this.photo = photo;
  
  this.game = game;
  this.x = x;
  this.y = y;

  this.marginLeft = 4;
  this.marginTop = 2;
  this.textPadding = 10;

  this.unlocked = unlocked;
  
  this.addImage(name);
  this.unlockedStyle(unlocked);
  this.addTitle(title);
  this.addDescription(description);

  this.game.add.existing(this);
}

var AchivmentsRowMixin = {
  addTitle: function(title) {
    var titleStyle = {
      font: "21px Jura",
      fill: "#000"
    }

    this.title = this.game.make.text(this.img.x + this.img.width + this.textPadding, this.img.y, title, titleStyle);

    this.addChild(this.title);
  },

  addDescription: function(description) {
    var descriptionStyle = {
      font: "16px Jura",
      fill: "#000"
    }

    this.description = this.game.make.text(this.img.x + this.img.width + this.textPadding, this.title.y + 20, description, descriptionStyle);
    this.description.wordWrap = true;
    if (this.unlocked)
      this.description.wordWrapWidth = (this.btn.x - this.btn.width) - (this.img.x + this.img.width) - this.textPadding * 2 - this.marginLeft * 2;
    else
      this.description.wordWrapWidth = this.width - (this.img.x + this.img.width) - this.textPadding * 2 - this.marginLeft * 2;

    this.description.lineSpacing = -5;

    this.addChild(this.description);
  },

  unlockedStyle: function(unlocked) {
    if (unlocked) {
      this.addUnlocked();      
    }
  },

  addUnlocked: function() {
    this.unlocked = this.game.make.sprite(this.width - 20 - this.marginLeft * 2, this.height / 2 - this.marginTop, 'achivments-unlocked');
    this.unlocked.anchor.setTo(1, 0.5);

    this.addShareBtn();

    this.addChild(this.unlocked);
  },

  addShareBtn: function() {
    this.btn = this.game.make.button(this.unlocked.x - 20 - this.unlocked.width, this.height / 2 - this.marginTop - 24 / 2, 'btnShareVk', this.btnShare_click, this);
    this.btn.anchor.setTo(1, 0);

    this.addChild(this.btn);
  },

  addImage: function(name) {
    this.filter = this.game.make.sprite(20 + this.marginLeft, 20 + this.marginTop, 'achivments-row-filter');
    this.img = this.game.make.sprite(20 + this.marginLeft, 20 + this.marginTop, name);
    this.img.width = this.filter.width;
    this.img.height = this.filter.height;

    this.addChild(this.img);
    this.addChild(this.filter);
  },

  btnShare_click: function() {
    VK.wall.postAchivment(this.photo);
  },

  hide: function() {
    this.game.add.tween(this).to({alpha: 0}, 300).start();
  }
}

AchivmentsRow.prototype = Object.create(Phaser.Sprite.prototype);

mixIt(AchivmentsRow, AchivmentsRowMixin);