/**
 * Данные о рейтинге
 */
[{
  id: Number,
  username: String,
  url: String
  score: Number,
  avatar: String,
  position: Number,
  isPlayer: Boolean
}]